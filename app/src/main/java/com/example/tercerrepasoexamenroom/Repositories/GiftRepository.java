package com.example.tercerrepasoexamenroom.Repositories;

import com.example.tercerrepasoexamenroom.Database.DAOGift;
import com.example.tercerrepasoexamenroom.Database.Gift;

import java.util.List;

public class GiftRepository {
    DAOGift daoGift;

    public GiftRepository(DAOGift daoGift){
        this.daoGift = daoGift;
    }

    public void insertGift(Gift gift){
        daoGift.insertGift(gift);
    }

    public List<Gift> sortExpensiveGifts(){
        return daoGift.getExpensiveGifts();
    }

    public List<Gift> getAllGifts(){
        return daoGift.getAllGifts();
    }

    public int[] getAllPrices(){
        return daoGift.getGiftPrices();
    }

    public int getAmountOfGifts(){
        return daoGift.getAmountOfGifts();
    }

    public void updateGifts(Gift gift){
        daoGift.updateGifts(gift);
    }

    public Gift findById(int id){
        return daoGift.findById(id);
    }

    public void deleteAllGifts(){
        daoGift.deleteAllGifts();
    }

    public void deleteSelectedGift(int id){
        daoGift.deleteSelectedGift(id);
    }
}
