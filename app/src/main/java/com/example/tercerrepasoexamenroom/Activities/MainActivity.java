package com.example.tercerrepasoexamenroom.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.tercerrepasoexamenroom.Adapters.MyAdapter;
import com.example.tercerrepasoexamenroom.Database.AppDataBase;
import com.example.tercerrepasoexamenroom.Database.DAOGift;
import com.example.tercerrepasoexamenroom.Database.Gift;
import com.example.tercerrepasoexamenroom.R;
import com.example.tercerrepasoexamenroom.Repositories.GiftRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static List<Gift> giftsList;
    static AppDataBase dataBase;
    static DAOGift daoGift;
    static GiftRepository repository;

    private FloatingActionButton addButton;
    private RecyclerView recyclerView;
    private boolean sortList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        addButton = findViewById(R.id.floatingActionButtonAdd);
        FloatingActionButton reorderButton = findViewById(R.id.floatingActionButtonReorder);

        dataBase = AppDataBase.getINSTANCE(getApplicationContext());

        daoGift = dataBase.daoGift();
        repository = new GiftRepository(daoGift);

        cleanRepository();

        giftsList = repository.getAllGifts();

        if (giftsList.isEmpty()) loadGifts();

        calculateGiftPrices();

        addButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, AddGiftActivity.class);
            startActivity(intent);
        });

        sortList = false;

        reorderButton.setOnClickListener(v -> {
            sortList =! sortList;

            if (sortList){
                giftsList = repository.sortExpensiveGifts();
            }else{
                giftsList = repository.getAllGifts();
            }
            setAdapter();
        });

        setAdapter();
    }

    public void setAdapter(){
        MyAdapter adapter = new MyAdapter(giftsList, R.layout.item_view);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        Si no funciona getApplicationContext() cambiarlo por this
        recyclerView.setLayoutManager(layoutManager);
    }

    public void loadGifts(){
        repository.insertGift(new Gift("PS5", 100, false));

        giftsList = repository.getAllGifts();
    }

    public void calculateGiftPrices(){
        int [] listOfGiftPrices = repository.getAllPrices();

        int valueOfGifts = 0, maxGiftsValue = 199;

        for (int giftPrice:
                listOfGiftPrices) {
            valueOfGifts += giftPrice;
        }

        if (valueOfGifts > maxGiftsValue){
            addButton.setVisibility(View.INVISIBLE);
        }else {
            addButton.setVisibility(View.VISIBLE);
        }
    }

    public void cleanRepository(){
        int maxGifts = 5, numberOfGifts = repository.getAmountOfGifts();
        if (numberOfGifts > maxGifts){
            repository.deleteAllGifts();
            Toast.makeText(getApplicationContext(), "Limpiando el repositorio", Toast.LENGTH_SHORT).show();
        }
    }
}