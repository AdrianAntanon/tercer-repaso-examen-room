package com.example.tercerrepasoexamenroom.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = Gift.class, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    public static AppDataBase INSTANCE;

    public abstract DAOGift daoGift();

    public static AppDataBase getINSTANCE(Context context){
        if (INSTANCE==null){
            INSTANCE = Room.databaseBuilder(context, AppDataBase.class, "Regalos.db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
