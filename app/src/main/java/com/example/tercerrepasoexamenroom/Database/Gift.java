package com.example.tercerrepasoexamenroom.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Gift {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "gift_id")
    private int idGift;

    private String giftName;
    private int giftPrice;
    private boolean giftStatus;

    public Gift(String giftName, int giftPrice, boolean giftStatus) {
        this.giftName = giftName;
        this.giftPrice = giftPrice;
        this.giftStatus = giftStatus;
    }

    public int getIdGift() {
        return idGift;
    }

    public void setIdGift(int idGift) {
        this.idGift = idGift;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGiftPrice() {
        return giftPrice;
    }

    public void setGiftPrice(int giftPrice) {
        this.giftPrice = giftPrice;
    }

    public boolean isGiftStatus() {
        return giftStatus;
    }

    public void setGiftStatus(boolean giftStatus) {
        this.giftStatus = giftStatus;
    }
}
