package com.example.tercerrepasoexamenroom.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DAOGift {
    @Query("SELECT * FROM Gift WHERE gift_id == :id")
    public Gift findById(int id);

    @Query("SELECT * FROM Gift")
    public List<Gift> getAllGifts();

    @Query("SELECT * FROM Gift WHERE giftPrice >= 50")
    public List<Gift> getExpensiveGifts();

    @Query("SELECT giftPrice FROM Gift")
    public int[] getGiftPrices();

    @Query("SELECT COUNT(*) FROM Gift")
    public int getAmountOfGifts();

    @Query("DELETE FROM Gift")
    public void deleteAllGifts();

    @Query("DELETE FROM Gift WHERE gift_id == :id")
    public void deleteSelectedGift(int id);

    @Update
    public void updateGifts(Gift gift);

    @Insert
    public void insertGift(Gift gift);
}
